# -*- coding: iso8859-15 -*-
import numpy as np

SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

INIT_SCREEN = 'home_screen'

CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))


# Type of the object detector: `OpenCV_DNN`, `Caffe`, `TensorFlow`
OBJECT_DETECTOR = 'OpenCV_DNN'

ZEROMQ_PORT = 12345
ZWEOMQ_TIEOUT = 10000       # 10 sec

DEBUG = False

try:
    from local_settings import *
except ImportError:
    pass
