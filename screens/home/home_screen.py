import random
import cv2
import os
from kivy.clock import mainthread, Clock
from kivy.logger import Logger
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from screens.base import BaseScreen
from settings import DEBUG, COLORS
from utils.common import frame2buf
from utils.object_detect.object_detector_opencv_dnn import detect_object
# from utils.object_detect.object_detector_caffe import detect_object
# from utils.object_detect.object_detector_tf_yolo_tiny import detect_object
# from utils.object_detect.object_detector_tf_yolo_small import detect_object
from utils.lane_detect.lane_detector_tf import detect_lane
from widgets.dialog import FileChooserDialog, MessageBox
import time
import sys

if not DEBUG:
    from utils.zed import ZEDController

Builder.load_file(os.path.join(os.path.dirname(__file__), 'home_screen.kv'))


class HomeScreen(BaseScreen):

    show_back_button = False
    src_type = StringProperty()
    src = StringProperty('Please select source.')

    img_size = (0, 0)
    _cap = None
    _texture = None
    _event = None

    _show_vehicle = BooleanProperty(False)
    _show_human = BooleanProperty(False)

    _cnt = 0
    _skip = 5

    def on_src_changed(self):
        self.src_type = self.ids.src.get_value()
        if self.src_type == 'Video File':
            popup = FileChooserDialog(mode='file', extensions=['mp4', 'MP4', 'mpg', 'MPG'])
            popup.bind(on_confirm=self._on_video_file_selected)
            popup.open()
        else:
            self._cancel_event()
            if DEBUG:
                self.start_web_camera()
            else:
                self.start_zed_camera()

    def _on_video_file_selected(self, *args):
        self._cancel_event()
        self.src = args[1]
        self._cap = cv2.VideoCapture(self.src)
        ret, frame = self._cap.read()
        self._calibrate_image(frame)
        Logger.debug('HV: Starting video file - {}'.format(self.src))
        self.ids.panel.opacity = 1
        self._event = Clock.schedule_interval(lambda dt: self._start_video_file(), 1. / 60.)

    def _start_video_file(self):
        if self._cap.isOpened():
            ret, frame = self._cap.read()
            self._cnt = (self._cnt + 1) % self._skip
            if frame is not None:
                if self._cnt == 0:
                    self._process_frame(frame)
                return True

        self._cap = None
        self._event = None
        MessageBox(msg='Finished!').open()
        return False        # Cancel schedule

    def start_web_camera(self):
        Logger.debug('HV: Starting Web Camera...')
        self._cap = cv2.VideoCapture(0)
        ret, frame = self._cap.read()
        if ret:
            self._calibrate_image(frame)
            Logger.debug('HV: Starting video file - {}'.format(self.src))
            self._event = Clock.schedule_interval(lambda dt: self._capture_webcam(), 1. / 60.)
            self.ids.panel.opacity = 1
        else:
            Logger.error('HV: Cannot read from webcam!')
            MessageBox(title='Error', msg='No Webcam Found!').open()

    def _capture_webcam(self):
        ret, frame = self._cap.read()
        if ret:
            self._process_frame(frame)
        else:
            Logger.error('HV: Cannot read from webcam, disconnected?')
            MessageBox(title='Error', msg='Webcam is disconnected!').open()

    def start_zed_camera(self):
        Logger.debug('HV: Starting ZED Camera...')
        self._cap = ZEDController()

    def _process_frame(self, frame):
        """
        Process the image frame and draw result
        :param frame:
        :return:
        """
        # TODO: Process the frame here
        start = time.time()

        result_frame = detect_object(frame)
        frame = detect_lane(frame)

        end = time.time()
        sys.stdout.write("\r{:.3f}".format(end-start))
        sys.stdout.flush()

        # Compose the result frame
        for r in result_frame:
            distance = random.randint(1, 10)                    # TODO: Get correct distance value

            if r['object'] in {'car', 'bus', 'motorbike'}:
                if not self._show_vehicle:
                    continue
            elif r['object'] == 'person':
                if not self._show_human:
                    continue

            label = "{}: {}m ({:.1f}%)".format(r['object'], distance, r['confidence'] * 100)
            cv2.rectangle(frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)

        # Convert numpy frame to texture to display on the kivy APP.
        resized = cv2.resize(frame, self.img_size)
        self._texture = frame2buf(resized)
        self._update_texture()

        vehicles = len([r for r in result_frame if r['object'] in {'bus', 'car', 'motorbike'}])
        self.ids.vehicles.text = str(vehicles)

        next_vehicle = random.randint(10, 99)                       # FIXME: Get exact value here.
        self.ids.next_vehicle.text = '{} % {}'.format(next_vehicle, random.choice(['Left', 'Right']))

        distance_next_vehicle = random.randint(0, 100) / 10.        # FIXME: Get exact value here.
        self.ids.distance_next_vehicle.text = '{} Meter'.format(distance_next_vehicle)

        human = len([r for r in result_frame if r['object'] == 'person'])
        self.ids.human.text = str(human)

        next_human = random.randint(10, 99)                         # FIXME: Get exact value here.
        self.ids.next_human.text = '{} % {}'.format(next_human, random.choice(['Left', 'Right']))

        distance_next_human = random.randint(0, 100) / 10.          # FIXME: Get exact value here.
        self.ids.distance_next_human.text = '{} Meter'.format(distance_next_human)

        pass_lane = random.choice([True, False])                    # FIXME: Get exact value here.
        self.ids.pass_lane.text = 'Yes' if pass_lane else 'No'
        self.ids.pass_lane.color = [0, 1, 0, 1] if pass_lane else [1, 0, 0, 1]

        left_distance = random.randint(0, 100) / 10.                # FIXME: Get exact value here
        self.ids.left_distance.text = '{} cm'.format(left_distance)

        right_distance = random.randint(0, 100) / 10.               # FIXME: Get exact value here
        self.ids.right_distance.text = '{} cm'.format(right_distance)

    @mainthread
    def _update_texture(self, *args):
        self.ids.img.texture = self._texture

    def _calibrate_image(self, frame):
        """
        Calibrate the size of the image to be shown.
        :return:
        """
        h, w, _ = frame.shape
        self.orig_size = [w, h]
        width, height = self.ids.img_box.size
        if w / h > width / height:
            self.ids.img.width = width
            self.ids.img.height = h * width / w
        else:
            self.ids.img.height = height
            self.ids.img.width = w * height / h
        self.img_size = (int(self.ids.img.width), int(self.ids.img.height))

    def on_vehicles_toggled(self):
        self._show_vehicle = self.ids.switch_vehicle.get_value()

    def on_human_toggled(self):
        self._show_human = self.ids.switch_human.get_value()

    def _cancel_event(self):
        if self._event:
            self._event.cancel()
            self._event = None

    def on_pre_leave(self, *args):
        super(HomeScreen, self).on_pre_leave(*args)
        self._cancel_event()
