# -*- coding: iso8859-15 -*-

from kivy.uix.screenmanager import ScreenManager
from screens.error.error_screen import ErrorScreen
from screens.home.home_screen import HomeScreen

screens = {
    'home_screen': HomeScreen,
    'error_screen': ErrorScreen,
}


sm = ScreenManager()
