import os
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.uix.screenmanager import Screen
from widgets.dialog import LoadingDialog


Builder.load_file(os.path.join(os.path.dirname(__file__), 'base.kv'))


class BaseScreen(Screen):
    app = ObjectProperty()
    show_back_button = BooleanProperty(True)
    loading_dlg = None

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self.app = App.get_running_app()
        self.loading_dlg = LoadingDialog()

    def switch_screen(self, screen_name, direction=None):
        self.app.switch_screen(screen_name=screen_name, direction=direction)

    def on_btn_back(self):
        if self.show_back_button:
            self.switch_screen('home_screen', 'right')
