#Pedestrians/Vehicles Detector with ZED Stereo Vision Camera


## Components

- Windows PC with Intel Core-i7 Quad Core CPU
- ZED Stereo Vision Camera (https://www.stereolabs.com/zed)
- Nvidia GeForce GTX1060 Graphic Card


## Installation

### Install **Python3** on Windows 10.

**NOTE:**
 
- Python **3.5 x64** should be installed.
- Add the path of Python to `PATH` environment when installing.

### Install the latest version of Kivy on Windows 10

https://kivy.org/docs/installation/installation-windows.html

### Install CUDA **8.0** and cuDNN **v5.1** on Windows 10
https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html

### Install Caffe with CUDA enabled.

- Download the prebuilt binaries of Caffe from [here](https://ci.appveyor.com/api/projects/BVLC/caffe/artifacts/build/caffe.zip?branch=windows&job=Environment%3A%20MSVC_VERSION%3D14%2C%20WITH_NINJA%3D1%2C%20CMAKE_CONFIG%3DRelease%2C%20CMAKE_BUILD_SHARED_LIBS%3D0%2C%20PYTHON_VERSION%3D3%2C%20WITH_CUDA%3D1)

- Extract it to `D:\` and execute this on the terminal:
    
        python -m pip install -U pip
        python -m pip install Cython numpy scipy scikit-image matplotlib ipython h5py networkx nose pandas python-dateutil protobuf python-gflags pyyaml Pillow six

### Install Tensorflow with CUDA(GPU) or CPU 
Install Tensorflow after CUDA and cuDNN installed

- Ubuntu/Linux 64-bit, CPU only, Python 3.5
   
    `$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.1.0-cp35-cp35m-linux_x86_64.whl`

- Ubuntu/Linux 64-bit, GPU enabled, Python 3.5
    
    Requires CUDA toolkit 8.0 and CuDNN v5. For other versions, see "Installing from sources" below.
  
    `$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-0.12.1-cp35-cp35m-linux_x86_64.whl`

- Can change the version with tensorflow_gpu-1.1.0 ...

    `$ sudo pip3 install --upgrade $TF_BINARY_URL`


### Install Python Wrapper of ZED Stereo Vision Camera

Visit https://github.com/stereolabs/zed-python and follow the instruction.


### Install Python library of ZeroMQ Client.

    python -m pip install pyzmq 

Check https://github.com/valkjsaaa/Unity-ZeroMQ-Example to see how to build a NetMQ Server in Unity2017.
 