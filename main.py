import gc

from kivy.app import App
# Import after Kivy
import config_kivy
import widgets.factory_reg

import traceback
from kivy.base import ExceptionHandler, ExceptionManager
from kivy.logger import Logger
from kivy.uix.screenmanager import SlideTransition, NoTransition
from kivymd.theming import ThemeManager
from screens.screen_manager import screens, sm
from settings import INIT_SCREEN


class HVExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        Logger.exception(exception)
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(HVExceptionHandler())


class HorizonVisionApp(App):

    current_screen = None
    exception = None
    theme_cls = ThemeManager()

    def build(self):
        self.switch_screen(INIT_SCREEN)
        return sm

    def switch_screen(self, screen_name, direction=None, duration=.3):
        if sm.has_screen(screen_name):
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction, duration=duration)
            else:
                sm.transition = NoTransition()

            sm.switch_to(screen)
            Logger.info('HV: ===== Switched to `{}` screen ====='.format(screen_name))
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception


if __name__ == '__main__':

    app = HorizonVisionApp()
    app.run()
