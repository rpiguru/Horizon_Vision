# -*- coding: iso8859-15 -*-
import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'labeled_switch.kv'))


class LabeledSwitch(BoxLayout):
    key = StringProperty()
    title = StringProperty()

    def __init__(self, **kwargs):
        super(LabeledSwitch, self).__init__(**kwargs)
        self.register_event_type('on_changed')
        Clock.schedule_once(lambda dt: self._bind_child())

    def _bind_child(self):
        self.ids.switch.bind(active=self.on_switched)

    def set_value(self, val):
        self.ids.switch.active = val

    def get_value(self):
        return self.ids.switch.active

    def on_switched(self, *args):
        self.dispatch('on_changed')

    def on_changed(self):
        pass
