import os
from kivy.lang import Builder
from kivy.properties import NumericProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'input.kv'))


class NumericTextField(BoxLayout):

    value = NumericProperty(0)
    minimum = NumericProperty(0)
    maximum = NumericProperty(100)

    def __init__(self, **kwargs):
        super(NumericTextField, self).__init__(**kwargs)

    def on_plus(self):
        if self.value < self.maximum:
            self.value += 1

    def on_minus(self):
        if self.value > self.minimum:
            self.value -= 1

    def set_value(self, val):
        self.value = val
