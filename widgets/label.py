import os
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.scrollview import ScrollView

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'label.kv'))


class ScrollableLabel(ScrollView):
    text = StringProperty('')
    color = ListProperty([1, 1, 1, 1])
