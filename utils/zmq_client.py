#! /usr/local/bin/python3
import zmq
from kivy.logger import Logger
from settings import ZEROMQ_PORT, ZWEOMQ_TIEOUT


context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:{}".format(ZEROMQ_PORT))


def send_message(data):
    """
    Send JSON data to the ZeroMQ server built on Unity.
    :param data:
    :type data: dict
    :return:
    """
    Logger.debug('ZeroMQ: Sending message - {}'.format(data))
    socket.send_json(data)
    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)
    evt = dict(poller.poll(ZWEOMQ_TIEOUT))
    if evt and evt.get(socket) == zmq.POLLIN:
        response = socket.recv(zmq.NOBLOCK)
        Logger.debug('ZeroMQ: Response - {}'.format(response))
        return True
    else:
        Logger.error('ZeroMQ: Failed to send message')
