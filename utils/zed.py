import pyzed.camera as zcam
import pyzed.defines as sl
import pyzed.types as tp
import pyzed.core as core
import math
import numpy as np
import sys


class ZEDController:

    def __init__(self):
        self.zed = zcam.PyZEDCamera()
        self.init_params = zcam.PyInitParameters()
        self.init_params.depth_mode = sl.PyDEPTH_MODE.PyDEPTH_MODE_PERFORMANCE
        self.init_params.coordinate_units = sl.PyUNIT.PyUNIT_CENTIMETER
        self.runtime_parameters = zcam.PyRuntimeParameters()

    def initialize(self):
        err = self.zed.open(self.init_params)
        if err != tp.PyERROR_CODE.PySUCCESS:
            self.runtime_parameters.sensing_mode = sl.PySENSING_MODE.PySENSING_MODE_STANDARD
            return True
        else:
            print('Failed to open the ZED Camera!')
            return False

    def test_distance(self):
        # Capture 50 images and depth, then stop
        i = 0
        image = core.PyMat()
        depth = core.PyMat()
        point_cloud = core.PyMat()

        while i < 50:
            # A new image is available if grab() returns PySUCCESS
            if self.zed.grab(self.runtime_parameters) == tp.PyERROR_CODE.PySUCCESS:
                # Retrieve left image
                self.zed.retrieve_image(image, sl.PyVIEW.PyVIEW_LEFT)
                # Retrieve depth map. Depth is aligned on the left image
                self.zed.retrieve_measure(depth, sl.PyMEASURE.PyMEASURE_DEPTH)
                # Retrieve colored point cloud. Point cloud is aligned on the left image.
                self.zed.retrieve_measure(point_cloud, sl.PyMEASURE.PyMEASURE_XYZRGBA)

                # Get and print distance value in mm at the center of the image
                # We measure the distance camera - object using Euclidean distance
                x = round(image.get_width() / 2)
                y = round(image.get_height() / 2)
                err, point_cloud_value = point_cloud.get_value(x, y)

                distance = math.sqrt(point_cloud_value[0] * point_cloud_value[0] +
                                     point_cloud_value[1] * point_cloud_value[1] +
                                     point_cloud_value[2] * point_cloud_value[2])

                if not np.isnan(distance) and not np.isinf(distance):
                    distance = round(distance)
                    print("Distance to Camera at ({0}, {1}): {2} cm\n".format(x, y, distance))
                    # Increment the loop
                    i = i + 1
                else:
                    print("Can't estimate distance at this position, move the camera\n")
                sys.stdout.flush()

    def close(self):
        self.zed.close()


if __name__ == "__main__":

    z = ZEDController()
    if z.initialize():
        sys.exit(1)
    else:
        z.test_distance()
