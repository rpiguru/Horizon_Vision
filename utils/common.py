import cv2
from kivy.graphics.texture import Texture


def frame2buf(frame):
    buf_tmp = cv2.flip(frame, 0)
    buf = buf_tmp.tostring()
    h, w = frame.shape[:2]
    texture = Texture.create(size=(w, h), colorfmt='bgr')
    texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
    return texture
