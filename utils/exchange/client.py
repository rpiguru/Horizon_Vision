#! /usr/local/bin/python3
import zmq
import sys
import time
import random
context = zmq.Context()
socket = context.socket(zmq.PAIR)

address = "localhost"
port = "12345"
socket.connect("tcp://{}:{}".format(address, port))

socket.send("request")
while True:
    message = socket.recv()
    sys.stdout.write("\r" + message)
    sys.stdout.flush()
    time.sleep(0.5)
