#! /usr/local/bin/python3
import zmq
import time
import random
context = zmq.Context()
socket = context.socket(zmq.PAIR)

port = "12345"
socket.bind("tcp://*:{}".format(port))

response = socket.recv()
print(response)

while True:
    message = str(time.ctime())
    socket.send(message)
    time.sleep(1)
