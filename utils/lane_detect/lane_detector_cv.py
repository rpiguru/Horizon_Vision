import numpy as np
import cv2
import math
import os
import copy
import time
# from utils.lane_detect.camera import Camera
from utils.lane_detect.funcs import *


_cur_dir = os.path.dirname(os.path.realpath(__file__))


def detect_lane(image):
    h, w = image.shape[:2]
    img_size = (w, h)

    src = np.float32([[250, 700], [1200, 700], [550, 450], [750, 450]])
    dst = np.float32([[250, 700], [1200, 700], [300, 50], [1000, 50]])

    # ------------------------------------------- undistort -------------------------------------------
    canny = cv2.Canny(image, 50, 200, apertureSize=3)
    cv2.imshow("canny", canny)

    # ------------------------------------------- transfer -------------------------------------------
    trans = transform(image, src, dst, img_size)
    cv2.imshow("trans", trans)

    # ------------------------------------------- threshold -------------------------------------------
    red_threshed = threshold_Channel(channel_Isolate(trans, 'R'), (220, 255))
    V_threshed = threshold_Channel(channel_Isolate(trans, 'V'), (220, 255))
    Cb_tresh = threshold_Channel(channel_Isolate(trans, 'Cb'), (200, 255))
    HSV = cv2.cvtColor(trans, cv2.COLOR_RGB2HSV)
    yellow = cv2.inRange(HSV, (20, 100, 100), (100, 255, 255))
    cv2.imshow("yellow", yellow)

    sensitivity_1 = 60
    white = cv2.inRange(HSV, (0, 0, 255 - sensitivity_1), (255, 50, 255))
    cv2.imshow("w", white)
    cv2.imshow("w", white)

    sensitivity_2 = 60
    HSL = cv2.cvtColor(trans, cv2.COLOR_RGB2HLS)
    white_2 = cv2.inRange(HSL, (0, 255 - sensitivity_2, 0), (255, 255, sensitivity_2))
    cv2.imshow("w2", white_2)

    white_3 = cv2.inRange(trans, (200, 200, 200), (255, 255, 255))
    cv2.imshow("w3", white_3)

    bit_layer = red_threshed | V_threshed | Cb_tresh | yellow | white | white_2 | white_3

    cv2.imshow("bit", bit_layer)


    # ------------------------------------------- candiated lines -------------------------------------------
    left_line, right_line, head = find_lines(bit_layer)

    # ------------------------------------------- calculate curve -------------------------------------------
    fit_left, fit_right, curve = lane_curve(left_line, right_line)

    # ------------------------------------------- argument -------------------------------------------
    unwarped, area, rad_curve, polygon_points = impose_Lane_Area(fit_left, fit_right, trans, src, dst, img_size, curve)

    info = {
        'area': area,
        'rad_curve': rad_curve,  # radius
        'polygon_points': polygon_points,  # contour points
        'head': head  # direction and distance
    }
    return unwarped, info


if __name__ == '__main__':

    # ----------------------------------- test with images -----------------------------------
    folder = _cur_dir + '/samples/' + 'test_images_2'
    files = [os.path.join(folder, fn) for fn in os.listdir(folder) if os.path.splitext(fn)[1] == ".jpg"]

    for path in files:
        image = cv2.imread(path)

        lane_poly, lane_info = detect_lane(image)

        result = cv2.addWeighted(image, 1, lane_poly, 0.3, 0)

        cv2.imshow('result', result)
        cv2.waitKey(0)

    # ----------------------------------- test with video -----------------------------------
    cap = cv2.VideoCapture(os.path.join(_cur_dir, 'samples', 'sample.mp4'))

    polygon, last_lane_polygon = None, None
    info, last_lane_info = None, None
    while cap.isOpened():
        s_time = time.time()
        _, frame = cap.read()

        lane_poly, lane_info = detect_lane(frame)

        if lane_info['area'] < 250000:  # wrong detected lanes
            info = copy.deepcopy(last_lane_info)
            polygon = last_lane_polygon.copy()
        else:
            info = copy.deepcopy(lane_info)
            polygon = lane_poly.copy()

        if info is not None:
            if last_lane_info is None or 0.045 > cv2.matchShapes(info['polygon_points'], last_lane_info['polygon_points'], 1, 0.0):
                last_lane_polygon = polygon.copy()
                last_lane_info = copy.deepcopy(info)

            result = cv2.addWeighted(frame, 1, polygon, 0.3, 0)
        else:
            result = image.copy()

        cv2.imshow("lane", result)
        key = cv2.waitKey(0) & 0xFF
        if key == ord('q'):
            break
        print(time.time() - s_time)
    cap.release()
    cv2.destroyAllWindows()
