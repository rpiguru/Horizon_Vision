import cv2
import sys
import numpy as np
import os


class Camera:
    # Compute camera calibration matrix and distortion coefficients.
    def __init__(self):
        self.dataset = "camera_cal"
        self.nx = 9
        self.ny = 6
        self.img_sz = (720, 1280)

        self.mtx, self.dist = self.calibration()
        pass

    def calibration(self):

        objp = np.zeros((self.nx * self.ny, 3), np.float32)
        objp[:, :2] = np.mgrid[0:self.nx, 0:self.ny].T.reshape(-1, 2)

        objpoints = []
        imgpoints = []

        # Image List
        images = [os.path.join(self.dataset, fn) for fn in os.listdir(self.dataset) if os.path.splitext(fn)[1].lower() == '.jpg']

        # Step through the list and search for chessboard corners
        for idx, fname in enumerate(images):
            img = cv2.imread(fname)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # Find the chessboard corners
            ret, corners = cv2.findChessboardCorners(gray, (self.nx, self.ny), None)

            # If found, add object points, image points
            if ret:
                objpoints.append(objp)
                imgpoints.append(corners)

        if len(objpoints) == 0 or len(imgpoints) == 0:
            sys.stderr.write("Calibration Failed")

        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, self.img_sz, None, None)
        return mtx, dist

    def undistort(self, image):
        image = cv2.undistort(image, self.mtx, self.dist, None, self.mtx)
        return image


if __name__ == '__main__':
    cam = Camera()
    checker_dist = cv2.imread("./camera_cal/calibration2.jpg")
    checker_undist = cam.undistort(checker_dist)
    cv2.imshow("origin", checker_dist)
    cv2.imshow("undistort", checker_undist)
    cv2.waitKey(0)
