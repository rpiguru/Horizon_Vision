import cv2
import sys
import numpy as np
import math


def abs_sobel_thresh(image, orient='x', sobel_kernel=3, thresh=(0, 255)):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    assert (orient == 'x' or orient == 'y'), "Orientation must be x or y"

    if orient == 'x':
        sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
        abs_sobelx = np.absolute(sobelx)
        scaled_sobel = np.uint8(255 * abs_sobelx / np.max(abs_sobelx))
    else:
        sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel)
        abs_sobely = np.absolute(sobely)
        scaled_sobel = np.uint8(255 * abs_sobely / np.max(abs_sobely))

    grad_binary = np.zeros_like(scaled_sobel)
    grad_binary[(scaled_sobel >= thresh[0]) & (scaled_sobel <= thresh[1])] = 1

    return grad_binary


def mag_thresh(image, sobel_kernel=3, thresh=(0, 255)):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
    sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel)

    abs_sobelxy = np.power((np.power(sobelx, 2) + np.power(sobely, 2)), 0.5)

    scaled_sobel = np.uint8(255 * abs_sobelxy / np.max(abs_sobelxy))

    mag_binary = np.zeros_like(scaled_sobel)
    mag_binary[(scaled_sobel >= thresh[0]) & (scaled_sobel <= thresh[1])] = 1

    return mag_binary


def dir_threshold(image, sobel_kernel=3, thresh=(0, np.pi / 2)):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
    sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel)
    with np.errstate(divide='ignore', invalid='ignore'):
        absgraddir = np.absolute(np.arctan(sobely / sobelx))
        dir_binary = np.zeros_like(absgraddir)
        dir_binary[(absgraddir >= thresh[0]) & (absgraddir <= thresh[1])] = 1

    return dir_binary


def channel_Isolate(image, channel):
    # Takes in only RBG images
    if channel == 'R':
        return image[:, :, 0]

    elif channel == 'G':
        return image[:, :, 1]

    elif channel == 'B':
        return image[:, :, 2]

    elif channel == 'H':
        HSV = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        return HSV[:, :, 0]

    elif channel == 'S':
        HSV = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        return HSV[:, :, 1]

    elif channel == 'V':
        HSV = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        return HSV[:, :, 2]

    elif channel == 'L':
        HLS = cv2.cvtColor(image, cv2.COLOR_RGB2HLS)
        return HLS[:, :, 1]

    elif channel == 'Cb':
        YCrCb = cv2.cvtColor(image, cv2.COLOR_RGB2YCrCb)
        return YCrCb[:, :, 2]

    elif channel == 'U':
        LUV = cv2.cvtColor(image, cv2.COLOR_RGB2Lab)
        return LUV[:, :, 2]

    else:
        sys.stderr.write("Channel must be either R, G, B, H, S, V, L, Cb, U\n")


def threshold_Channel(channel, thresh):
    retval, binary = cv2.threshold(channel.astype('uint8'), thresh[0], thresh[1], cv2.THRESH_BINARY)
    return binary


def transform(undist, src, dst, img_size):
    M = cv2.getPerspectiveTransform(src, dst)
    warped = cv2.warpPerspective(undist, M, img_size)
    return warped


def find_lines(img):
    right_side_x = []
    right_side_y = []
    left_side_x = []
    left_side_y = []

    past_cord = 0

    # right side
    for i in reversed(range(10, 100)):
        histogram = np.sum(img[int(i * img.shape[0] / 100):int((i + 1) * img.shape[0] / 100), int(img.shape[1] / 2):], axis=0)
        xcord = int(np.argmax(histogram)) + 640
        ycord = int(i * img.shape[0] / 100)
        if i == 50:
            right_lane_dp = xcord
        if ycord == 0 or xcord == 0:
            pass
        elif abs(xcord - past_cord) > 100 and not (i == 99) and not (past_cord == 0):
            pass
        elif xcord == 640:
            pass
        else:
            right_side_x.append(xcord)
            right_side_y.append(ycord)
            past_cord = xcord

    past_cord = 0
    # left side
    for i in reversed(range(10, 100)):
        histogram = np.sum(img[int(i * img.shape[0] / 100):int((i + 1) * img.shape[0] / 100), : int(img.shape[1] / 2)], axis=0)
        xcord = int(np.argmax(histogram))
        ycord = int(i * img.shape[0] / 100)
        if i == 50:
            left_lane_dp = xcord
        if ycord == 0 or xcord == 0:
            pass
        elif abs(xcord - past_cord) > 100 and not (i == 99) and not (past_cord == 0):
            pass
        else:
            left_side_x.append(xcord)
            left_side_y.append(ycord)
            past_cord = xcord

    left_line = (left_side_x, left_side_y)
    right_line = (right_side_x, right_side_y)

    left_line = (left_line[0][1:(len(left_line[0]) - 1)], left_line[1][1:(len(left_line[1]) - 1)])
    right_line = (right_line[0][1:(len(right_line[0]) - 1)], right_line[1][1:(len(right_line[1]) - 1)])

    lane_middle = int((right_lane_dp - left_lane_dp) / 2.) + left_lane_dp

    if lane_middle - 640 > 0:
        leng = 3.66 / 2
        mag = (lane_middle - 640) / 640. * leng
        head = ("Right", mag)
    else:
        leng = 3.66 / 2.
        mag = ((lane_middle - 640) / 640. * leng) * -1
        head = ("Left", mag)

    return left_line, right_line, head


def lane_curve(left_line, right_line):
    degree_fit = 2

    fit_left = np.polyfit(left_line[1], left_line[0], degree_fit)

    fit_right = np.polyfit(right_line[1], right_line[0], degree_fit)

    x = [x * (3.7 / 700.) for x in left_line[0]]
    y = [x * (30 / 720.) for x in left_line[1]]

    curve = np.polyfit(y, x, degree_fit)

    return fit_left, fit_right, curve


def impose_Lane_Area(fit_left, fit_right, trans, src, dst, img_size, curve):
    global left_points, right_points

    left_points = []
    right_points = []

    left = np.poly1d(fit_left)
    right = np.poly1d(fit_right)

    rad_curve = ((1 + (2 * curve[0] * 710 / 2 + curve[1]) ** 2) ** 1.5) / np.absolute(2 * curve[0])

    for i in range(100, 710, 2):
        if int(left(i) < 0):
            pass
        else:
            left_points.append([int(left(i)), i])

    for i in range(100, 710, 2):
        if int(right(i) < 0):
            pass
        else:
            right_points.append([int(right(i)), i])

    polygon_points = right_points + list(reversed(left_points))
    polygon_points = np.array(polygon_points)

    overlay = np.zeros_like(trans)
    trans_image = cv2.fillPoly(overlay, [polygon_points], (0, 255, 0))

    M = cv2.getPerspectiveTransform(dst, src)
    unwarped = cv2.warpPerspective(trans_image, M, img_size)

    area = cv2.contourArea(polygon_points)

    # area = 10000000000
    return unwarped, area, rad_curve, polygon_points
