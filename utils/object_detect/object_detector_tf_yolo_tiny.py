import os
import numpy as np
import cv2
import tensorflow as tf


CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

_cur_dir = os.path.dirname(os.path.realpath(__file__))


# ========== Select Model ========
MODEL_TYPE = 'YOLO_tiny'


# ========== Define net & corresponding dimension
WEIGHTS = os.path.join(_cur_dir, 'models', 'YOLO_tiny.ckpt')
DIMENSION = (448, 448)


class YoloTinyUtils:
    def __init__(self):
        self.weights = WEIGHTS

        self.alpha = 0.1
        self.threshold = 0.1
        self.iou_threshold = 0.5

        self.__build_networks()

    def __build_networks(self):
        print("building yolo small graph...")
        self.x = tf.placeholder('float32', [None, 448, 448, 3])
        self.conv_1 = self.conv_layer(1, self.x, 16, 3, 1)
        self.pool_2 = self.pooling_layer(2, self.conv_1, 2, 2)
        self.conv_3 = self.conv_layer(3, self.pool_2, 32, 3, 1)
        self.pool_4 = self.pooling_layer(4, self.conv_3, 2, 2)
        self.conv_5 = self.conv_layer(5, self.pool_4, 64, 3, 1)
        self.pool_6 = self.pooling_layer(6, self.conv_5, 2, 2)
        self.conv_7 = self.conv_layer(7, self.pool_6, 128, 3, 1)
        self.pool_8 = self.pooling_layer(8, self.conv_7, 2, 2)
        self.conv_9 = self.conv_layer(9, self.pool_8, 256, 3, 1)
        self.pool_10 = self.pooling_layer(10, self.conv_9, 2, 2)
        self.conv_11 = self.conv_layer(11, self.pool_10, 512, 3, 1)
        self.pool_12 = self.pooling_layer(12, self.conv_11, 2, 2)
        self.conv_13 = self.conv_layer(13, self.pool_12, 1024, 3, 1)
        self.conv_14 = self.conv_layer(14, self.conv_13, 1024, 3, 1)
        self.conv_15 = self.conv_layer(15, self.conv_14, 1024, 3, 1)
        self.fc_16 = self.fc_layer(16, self.conv_15, 256, flat=True, linear=False)
        self.fc_17 = self.fc_layer(17, self.fc_16, 4096, flat=False, linear=False)
        # skip dropout_18
        self.fc_19 = self.fc_layer(19, self.fc_17, 1470, flat=False, linear=True)
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer()) # self.sess.run(tf.initialize_all_variables())
        self.saver = tf.train.Saver()
        self.saver.restore(self.sess, self.weights)
        print("completed the config yolo small!")

    def conv_layer(self, idx, inputs, filters, size, stride):
        channels = inputs.get_shape()[3]
        weight = tf.Variable(tf.truncated_normal([size, size, int(channels), filters], stddev=0.1))
        biases = tf.Variable(tf.constant(0.1, shape=[filters]))

        pad_size = size // 2
        pad_mat = np.array([[0, 0], [pad_size, pad_size], [pad_size, pad_size], [0, 0]])
        inputs_pad = tf.pad(inputs, pad_mat)

        conv = tf.nn.conv2d(inputs_pad, weight, strides=[1, stride, stride, 1], padding='VALID',
                            name=str(idx) + '_conv')
        conv_biased = tf.add(conv, biases, name=str(idx) + '_conv_biased')
        # print('    Layer  %d : Type = Conv, Size = %d * %d, Stride = %d, Filters = %d, Input channels = %d' % (
        # idx, size, size, stride, filters, int(channels)))
        return tf.maximum(self.alpha * conv_biased, conv_biased, name=str(idx) + '_leaky_relu')

    def pooling_layer(self, idx, inputs, size, stride):
        # print('    Layer  %d : Type = Pool, Size = %d * %d, Stride = %d' % (idx, size, size, stride))
        return tf.nn.max_pool(inputs, ksize=[1, size, size, 1], strides=[1, stride, stride, 1], padding='SAME',
                              name=str(idx) + '_pool')

    def fc_layer(self, idx, inputs, hiddens, flat=False, linear=False):
        input_shape = inputs.get_shape().as_list()
        if flat:
            dim = input_shape[1] * input_shape[2] * input_shape[3]
            inputs_transposed = tf.transpose(inputs, (0, 3, 1, 2))
            inputs_processed = tf.reshape(inputs_transposed, [-1, dim])
        else:
            dim = input_shape[1]
            inputs_processed = inputs
        weight = tf.Variable(tf.truncated_normal([dim, hiddens], stddev=0.1))
        biases = tf.Variable(tf.constant(0.1, shape=[hiddens]))
        # print('    Layer  %d : Type = Full, Hidden = %d, Input dimension = %d, Flat = %d, Activation = %d'
        # % (idx, hiddens, int(dim), int(flat), 1 - int(linear)))
        if linear:
            return tf.add(tf.matmul(inputs_processed, weight), biases, name=str(idx) + '_fc')
        ip = tf.add(tf.matmul(inputs_processed, weight), biases)
        return tf.maximum(self.alpha * ip, ip, name=str(idx) + '_fc')

    def iou(self, box1, box2):
        tb = min(box1[0]+0.5*box1[2],box2[0]+0.5*box2[2])-max(box1[0]-0.5*box1[2],box2[0]-0.5*box2[2])
        lr = min(box1[1] + 0.5 * box1[3], box2[1] + 0.5 * box2[3]) - max(box1[1] - 0.5 * box1[3], box2[1] - 0.5 * box2[3])
        if tb < 0 or lr < 0:
            intersection = 0
        else:
            intersection = tb * lr
        return intersection / (box1[2] * box1[3] + box2[2] * box2[3] - intersection)

    def interpret_output(self, output):
        probs = np.zeros((7, 7, 2, 20))
        class_probs = np.reshape(output[0:980], (7, 7, 20))
        scales = np.reshape(output[980:1078], (7, 7, 2))
        boxes = np.reshape(output[1078:], (7, 7, 2, 4))
        offset = np.transpose(np.reshape(np.array([np.arange(7)] * 14), (2, 7, 7)), (1, 2, 0))

        boxes[:, :, :, 0] += offset
        boxes[:, :, :, 1] += np.transpose(offset, (1, 0, 2))
        boxes[:, :, :, 0:2] = boxes[:, :, :, 0:2] / 7.0
        boxes[:, :, :, 2] = np.multiply(boxes[:, :, :, 2], boxes[:, :, :, 2])
        boxes[:, :, :, 3] = np.multiply(boxes[:, :, :, 3], boxes[:, :, :, 3])

        # boxes[:, :, :, 0] *= self.w_img
        # boxes[:, :, :, 1] *= self.h_img
        # boxes[:, :, :, 2] *= self.w_img
        # boxes[:, :, :, 3] *= self.h_img

        for i in range(2):
            for j in range(20):
                probs[:, :, i, j] = np.multiply(class_probs[:, :, j], scales[:, :, i])

        filter_mat_probs = np.array(probs >= self.threshold, dtype='bool')
        filter_mat_boxes = np.nonzero(filter_mat_probs)
        boxes_filtered = boxes[filter_mat_boxes[0], filter_mat_boxes[1], filter_mat_boxes[2]]
        probs_filtered = probs[filter_mat_probs]
        classes_num_filtered = np.argmax(filter_mat_probs, axis=3)[
            filter_mat_boxes[0], filter_mat_boxes[1], filter_mat_boxes[2]]

        argsort = np.array(np.argsort(probs_filtered))[::-1]
        boxes_filtered = boxes_filtered[argsort]
        probs_filtered = probs_filtered[argsort]
        classes_num_filtered = classes_num_filtered[argsort]

        for i in range(len(boxes_filtered)):
            if probs_filtered[i] == 0:
                continue
            for j in range(i + 1, len(boxes_filtered)):
                if self.iou(boxes_filtered[i], boxes_filtered[j]) > self.iou_threshold:
                    probs_filtered[j] = 0.0

        filter_iou = np.array(probs_filtered > 0.0, dtype='bool')
        boxes_filtered = boxes_filtered[filter_iou]
        probs_filtered = probs_filtered[filter_iou]
        classes_num_filtered = classes_num_filtered[filter_iou]

        result = []
        for i in range(len(boxes_filtered)):
            result.append(
                [classes_num_filtered[i], boxes_filtered[i][0], boxes_filtered[i][1], boxes_filtered[i][2],
                 boxes_filtered[i][3], probs_filtered[i]])

        return result

    def detect(self, frame):
        yolo_img = frame.copy()
        img_resized = cv2.resize(yolo_img, (448, 448))
        img_RGB = cv2.cvtColor(img_resized, cv2.COLOR_BGR2RGB)
        img_resized_np = np.asarray(img_RGB)
        inputs = np.zeros((1, 448, 448, 3), dtype='float32')
        inputs[0] = (img_resized_np / 255.0) * 2.0 - 1.0
        in_dict = {self.x: inputs}
        net_output = self.sess.run(self.fc_19, feed_dict=in_dict)

        detections = self.interpret_output(net_output[0])
        return detections


yolo_tf = YoloTinyUtils()


def detect_object(frame, confidence=.5):
    img_h, img_w = frame.shape[:2]

    detections = yolo_tf.detect(frame=frame)
    results = []
    for i in range(len(detections)):
        _label_idx = detections[i][0]
        _confidence = detections[i][-1]
        if _confidence > confidence:
            x0, y0, w, h = detections[i][1:-1]
            (x, y, w, h) = (x0 - w / 2, y0 - h / 2, w, h) * np.array([img_w, img_h, img_w, img_h])

            results.append(dict(
                index=i,
                object=CLASSES[_label_idx + 1],
                confidence=_confidence,
                rect=[int(x), int(y), int(x + w), int(y + h)]
            ))
    return results


if __name__ == '__main__':

    import glob
    import pprint
    import time

    COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

    # Test with images
    for img in glob.glob(os.path.join(_cur_dir, 'samples', '*.jpg')):
        _frame = cv2.imread(img)
        _result = detect_object(_frame, confidence=.2)
        print('=== Detecting {} ==='.format(img))
        pprint.pprint(_result)

        for r in _result:
            label = "{}: {:.2f}%".format(r['object'], r['confidence'] * 100)
            cv2.rectangle(_frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(_frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)
        cv2.imshow("Output", _frame)
        cv2.waitKey(0)

    # Test with video file
    cap = cv2.VideoCapture(os.path.join(_cur_dir, 'samples', 'sample.mp4'))
    while cap.isOpened():
        s_time = time.time()
        ret, _frame = cap.read()
        _result = detect_object(_frame, confidence=.2)
        for r in _result:
            label = "{}: {:.2f}%".format(r['object'], r['confidence'] * 100)
            cv2.rectangle(_frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(_frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)
        cv2.imshow("Output", _frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        print(time.time() - s_time)

    cap.release()
    cv2.destroyAllWindows()
