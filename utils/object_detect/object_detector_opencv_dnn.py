import os
import numpy as np
import cv2


CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

_cur_dir = os.path.dirname(os.path.realpath(__file__))


# ========== Select Model ========
MODEL_TYPE = 'MobileNet-SSD'
# MODEL_TYPE = 'VGG-VOC'


# ========== Define net & corresponding dimension
if MODEL_TYPE == 'MobileNet-SSD':
    net = cv2.dnn.readNetFromCaffe(
        os.path.join(_cur_dir, 'models', 'MobileNetSSD_deploy.prototxt.txt'),
        os.path.join(_cur_dir, 'models', 'MobileNetSSD_deploy.caffemodel'))
    DIMENSION = (512, 512)
else:
    net = cv2.dnn.readNetFromCaffe(
        os.path.join(_cur_dir, 'models', 'VGG_VOC0712Plus_SSD_512x512_ft_iter_160000_deploy.prototxt'),
        os.path.join(_cur_dir, 'models', 'VGG_VOC0712Plus_SSD_512x512_ft_iter_160000.caffemodel'))
    DIMENSION = (512, 512)


def detect_object(frame, confidence=.5):

    h, w = frame.shape[:2]
    if MODEL_TYPE == 'MobileNet-SSD':
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, DIMENSION), 0.007843, DIMENSION, 127.5)
    else:
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, DIMENSION), 1, DIMENSION)

    net.setInput(blob)
    detections = net.forward()

    result = []

    for i in np.arange(0, detections.shape[2]):
        _confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is greater than the minimum confidence
        if _confidence > confidence:
            # extract the index of the class label from the `detections`,
            # then compute the (x, y)-coordinates of the bounding box for the object
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            result.append(dict(
                index=idx,
                object=CLASSES[idx],
                confidence=_confidence,
                rect=[startX, startY, endX, endY]
            ))

    return result


if __name__ == '__main__':

    import glob
    import pprint
    import time

    COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

    # Test with images
    for img in glob.glob(os.path.join(_cur_dir, 'samples', '*.jpg')):
        _frame = cv2.imread(img)
        _result = detect_object(_frame, confidence=.5)
        print('=== Detecting {} ==='.format(img))
        pprint.pprint(_result)

        for r in _result:
            label = "{}: {:.2f}%".format(r['object'], r['confidence'] * 100)
            cv2.rectangle(_frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(_frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)
        cv2.imshow("Output", _frame)
        cv2.waitKey(0)

    # Test with video file
    cap = cv2.VideoCapture(os.path.join(_cur_dir, 'samples', 'sample.mp4'))
    while cap.isOpened():
        s_time = time.time()
        ret, _frame = cap.read()
        _result = detect_object(_frame, confidence=.5)
        for r in _result:
            label = "{}: {:.2f}%".format(r['object'], r['confidence'] * 100)
            cv2.rectangle(_frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(_frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)
        cv2.imshow("Output", _frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        print(time.time() - s_time)

    cap.release()
    cv2.destroyAllWindows()
