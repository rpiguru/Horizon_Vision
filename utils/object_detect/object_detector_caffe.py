import os
import platform
import sys
import cv2
import numpy as np


if platform.system() == 'Windows':
    sys.path.append('D:\caffe\python')
else:
    sys.path.append(os.path.expanduser('~/caffe-rc5-py3/python'))

import caffe


CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

_cur_dir = os.path.dirname(os.path.realpath(__file__))


# ========== Select Model ========
MODEL_TYPE = 'MobileNet-SSD'
# MODEL_TYPE = 'VGG_VOC0712Plus-SSD'


# ========== Define net & corresponding dimension
if MODEL_TYPE == 'MobileNet-SSD':
    net = caffe.Net(os.path.join(_cur_dir, 'models', 'MobileNetSSD_deploy.prototxt.txt'),
                    os.path.join(_cur_dir, 'models', 'MobileNetSSD_deploy.caffemodel'), caffe.TEST)
    # net = caffe.Net(os.path.expanduser('~/Downloads/MobileNetSSD_deploy.prototxt'),
    #                 os.path.expanduser('~/Downloads/MobileNetSSD_deploy.caffemodel'), caffe.TEST)
    DIMENSION = (300, 300)
    OFFSET = 127.5
    RATE = .007843
else:
    net = caffe.Net(
        os.path.join(_cur_dir, 'models', 'VGG_VOC0712Plus_SSD_512x512_ft_iter_160000_deploy.prototxt'),
        os.path.join(_cur_dir, 'models', 'VGG_VOC0712Plus_SSD_512x512_ft_iter_160000.caffemodel'), caffe.TEST)
    DIMENSION = (512, 512)
    OFFSET = 0
    RATE = 1.


def _post_process(frame, out):
    h, w, _ = frame.shape
    box = out['detection_out'][0, 0, :, 3:7] * np.array([w, h, w, h])
    cls = out['detection_out'][0, 0, :, 1]
    conf = out['detection_out'][0, 0, :, 2]
    return box.astype(np.int32), conf, cls


def detect_object(frame, confidence=.5):
    h, w, _ = frame.shape
    resized = cv2.resize(frame, DIMENSION)
    resized = resized - OFFSET
    resized = resized * RATE
    img = resized.astype(np.float32)
    img = img.transpose((2, 0, 1))
    net.blobs['data'].data[...] = img
    out = net.forward()
    box, conf, cls = _post_process(frame, out)
    result = []
    for i, b in enumerate(box):
        if conf[i] > confidence:
            cv2.rectangle(frame, (b[0], b[1]), (b[2], b[3]), (0, 255, 0))
            result.append(dict(
                index=int(cls[i]),
                object=CLASSES[int(cls[i])],
                confidence=conf[i],
                rect=b
            ))
    return result


if __name__ == '__main__':

    import glob
    import pprint
    import time

    COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

    # Test with images
    for _img in glob.glob(os.path.join(_cur_dir, 'samples', '*.jpg')):
        _frame = cv2.imread(_img)
        _result = detect_object(_frame, confidence=.5)
        print('=== Detecting {} ==='.format(_img))
        pprint.pprint(_result)

        for r in _result:
            label = "{}: {:.2f}%".format(r['object'], r['confidence'] * 100)
            cv2.rectangle(_frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(_frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)
        cv2.imshow("Outputq", _frame)
        cv2.waitKey(0)

    # Test with video file
    cap = cv2.VideoCapture(os.path.join(_cur_dir, 'samples', 'sample.mp4'))
    while cap.isOpened():
        s_time = time.time()
        ret, _frame = cap.read()
        _result = detect_object(_frame, confidence=.5)
        for r in _result:
            label = "{}: {:.2f}%".format(r['object'], r['confidence'] * 100)
            cv2.rectangle(_frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(_frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)
        cv2.imshow("Output", _frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        print(time.time() - s_time)

    cap.release()
    cv2.destroyAllWindows()
