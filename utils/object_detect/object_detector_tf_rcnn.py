import os
import numpy as np
import cv2
import tensorflow as tf


CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

_cur_dir = os.path.dirname(os.path.realpath(__file__))

import numpy as np
import os
import tensorflow as tf
import cv2
import time


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

_cur_dir = os.path.dirname(os.path.realpath(__file__))


class TfRcnnUtils:
    def __init__(self):
        models = [
            'rfcn_resnet101_coco_2018_01_28',
            'faster_rcnn_resnet101_coco_2018_01_28',
            'faster_rcnn_inception_resnet_v2_atrous_coco_2018_01_28',
            'faster_rcnn_resnet101_coco_2018_01_28.tar.gz',
            'faster_rcnn_resnet101_lowproposals_coco_2018_01_28.tar.gz'
        ]
        self.model_dir = models[0]
        self.path_to_ckpt = _cur_dir + '/models/cnns/' + self.model_dir + '/frozen_inference_graph.pb'
        self.__load_model()

    def __load_model(self):
        # Load model into memory
        print('Loading model...')
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        print('configure model...')
        self.sess = None
        with detection_graph.as_default():
            with tf.Session(graph=detection_graph) as self.sess:
                self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
                self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
                self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    def detect(self, image, confidence=0.5):

        height, width = image.shape[:2]
        image_np_expanded = np.expand_dims(img, axis=0)

        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})

        result = []
        for i in range(boxes.shape[1]):
            idx = int(classes[0][i])
            _confidence = scores[0][i]
            if scores[0][i] < confidence:
                continue

            y_min, x_min, y_max, x_max = boxes[0][i] * np.array([width, height, width, height])
            rect = [x_min * width, y_min * height, x_max * width, y_max * height]
            result.append(dict(
                index=idx,
                object=CLASSES[idx],
                confidence=_confidence,
                rect=rect
            ))

        return result


if __name__ == '__main__':

    cap = cv2.VideoCapture("sample.mp4")

    obj = TfRcnnUtils()

    # Test with video file
    cap = cv2.VideoCapture(os.path.join(_cur_dir, 'samples', 'sample.mp4'))
    while cap.isOpened():
        s_time = time.time()
        ret, _frame = cap.read()
        _result = detect_object(_frame, confidence=.2)
        for r in _result:
            label = "{}: {:.2f}%".format(r['object'], r['confidence'] * 100)
            cv2.rectangle(_frame, (r['rect'][0], r['rect'][1]), (r['rect'][2], r['rect'][3]), COLORS[r['index']], 2)
            y = r['rect'][1] - 15 if r['rect'][1] - 15 > 15 else r['rect'][1] + 15
            cv2.putText(_frame, label, (r['rect'][0], y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[r['index']], 2)
        cv2.imshow("Output", _frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        print(time.time() - s_time)

    cap.release()
    cv2.destroyAllWindows()
