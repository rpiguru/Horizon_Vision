"""
    Pre-configuration file to be used to setup Kivy configurations.
    This module must be called before executing a Kivy GUI app!
"""
import os
from kivy.clock import Clock
from kivy.config import Config
from settings import SCREEN_WIDTH, SCREEN_HEIGHT

Config.read(os.path.expanduser('~/.kivy/config.ini'))

Config.set('graphics', 'width', str(SCREEN_WIDTH))
Config.set('graphics', 'height', str(SCREEN_HEIGHT))
Config.set('kivy', 'keyboard_mode', 'system')
Config.set('kivy', 'keyboard_layout', 'en_US')
Config.set('kivy', 'log_level', 'debug')

Clock.max_iteration = 20
